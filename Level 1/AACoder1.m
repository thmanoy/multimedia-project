function AACSeq1 = AACoder1(fNameIn)
% X will be a Mx2 matrix, Fs the sample rate
[X, ~] = audioread(fNameIn);
[M, ~] = size(X);
% Find missing samples of last frame
K = 2048-mod(M,2048);
%Pad zeros to have complete frames of 2048
X=padarray(X,[K 0],0,'post');
X=[zeros(2048,2);X];
%Find no of sample windows
K = ceil(M/2048);
K=K+1;
%Initialize AACSeq1 struct
AACSeq1 = struct;
for i=1:((2*K)-1)  
    %Determine FrameT and nextFrameT
     frameT=X((i-1)*1024+1:(i+1)*1024,:);
    %Check if it is the last frame
    if(i~=((2*K)-1))
        nextFrameT=X(i*1024+1:(i+2)*1024,:);
    else
        % There is no next Frame
        nextFrameT = zeros(2048,2);
    end
    %Determine prevFrameType
    if i == 1
        % There is no previous frame, but EHS has high energy, so I suppose it's OLS (low energy)
        prevFrameType = 'ESH';
    else
        prevFrameType = AACSeq1(i-1).frameType;
    end  
    %Determine Current Frame Type from SSC
    AACSeq1(i).frameType = SSC(frameT, nextFrameT, prevFrameType);
    %Set winType as sinusoid
    AACSeq1(i).winType = 'KBD';
    %Calculate Frame in Frequency
    frameF = filterbank(frameT, AACSeq1(i).frameType, AACSeq1(i).winType);
    if strcmp(AACSeq1(i).frameType, 'ESH')
        AACSeq1(i).chl.frameF = frameF(:,1,:);
        AACSeq1(i).chr.frameF = frameF(:,2,:);
    else
        AACSeq1(i).chl.frameF = frameF(:, 1);
        AACSeq1(i).chr.frameF = frameF(:, 2);
    end
end
end

% frameType can only be OLS, LSS, ESH, LPS
function frameType = SSC(frameT, nextFrameT, prevFrameType)
if strcmp(prevFrameType, 'LSS')
    frameType = 'ESH';
elseif strcmp(prevFrameType, 'LPS')
    frameType = 'OLS';
else
    b = [0.7548 -0.7548];
    a = [1 -0.5095];
    nextFrameT(:, 1) = filter(b, a, nextFrameT(:,1));
    nextFrameT(:, 2) = filter(b, a, nextFrameT(:,2));
    s2 = zeros(8,2);
    [yu, ya] = size(nextFrameT);
    if yu ~= 2048
        nextFrameT = [nextFrameT; zeros(2048-yu, 2)];
    end
    for i=1:8
        for j=1:128
            s2(i, 1) = s2(i, 1) + nextFrameT(448+128+j+128*(i-1))^2;
            s2(i, 2) = s2(i, 2) + nextFrameT(448+128+j+128*(i-1))^2;
        end
    end
    ds2 = zeros(7,2);
    for l=1:7
        den1 = 0;
        den2 = 0;
        for m=1:l
            den1 = den1 + s2(m, 1);
            den2 = den2 + s2(m, 2);
        end
        ds2(l, 1) = l*s2(l+1,1)/den1;
        ds2(l, 2) = l*s2(l+1,2)/den2;
    end
    % Frame 1:
    nextFrameType1 = 'Def'; nextFrameType2 = 'Def';
    for l=1:7
        if ds2(l,1)>10 && s2(l+1,1)>0.001
            nextFrameType1 = 'ESH';
            break;
        end
    end
    for l=1:7
        if ds2(l,2)>10 && s2(l,2)>0.001
            nextFrameType2 = 'ESH';
            break;
        end
    end
    if strcmp(prevFrameType, 'OLS')
        if strcmp(nextFrameType1, 'ESH')
            frameType1 = 'LSS';
        else
            frameType1 = 'OLS';
        end
        if strcmp(nextFrameType2, 'ESH')
            frameType2 = 'LSS';
        else
            frameType2 = 'OLS';
        end
    end
    if strcmp(prevFrameType, 'ESH')
        if strcmp(nextFrameType1, 'ESH')
            frameType1 = 'ESH';
        else
            frameType1 = 'LPS';
        end
        if strcmp(nextFrameType2, 'ESH')
            frameType2 = 'ESH';
        else
            frameType2 = 'LPS';
        end
    end
%     fprintf('%s PP %s PP %s\n' , prevFrameType, nextFrameType1, nextFrameType2);
    if strcmp(frameType1,'ESH') || strcmp(frameType2,'ESH') || (strcmp(frameType1,'LSS') && strcmp(frameType2,'LPS')) || (strcmp(frameType1,'LPS') && strcmp(frameType2,'LSS')) 
        frameType='ESH';
    elseif strcmp(frameType1,'OLS') && strcmp(frameType2,'OLS')
        frameType='OLS';
    elseif strcmp(frameType1,'LPS') || strcmp(frameType2,'LPS')
        frameType = 'LPS';
    else
        frameType = 'LSS';
    end
end
end
function frameF = filterbank(frameT, frameType, winType)
frameInTime=frameT;

%a. Frame Type : Only Long Sequence 
if strcmp(frameType,'OLS') 
    %Determine Window
    if strcmp(winType,'KBD')    %KBD Window
      w=[KBDL(2048) ; KBDR(2048)];
    else                                    %SIN Window
      w=sinewin(2048);
    end
    %Multiply Frame with Window
    frameInTime(:,1)=frameInTime(:,1).*w;
    frameInTime(:,2)=frameInTime(:,2).*w;
    %initialize frameF
    frameF=zeros(1024,2);
    %Calculate MDCT 
    frameF(:,1)=mdct4(frameInTime(:,1));
    frameF(:,2)=mdct4(frameInTime(:,2));
    
%b. Frame Type : Long Start Sequence     
elseif strcmp(frameType,'LSS')
    %Determine Window
     w=zeros(2048,1);
     if strcmp(winType,'KBD')    %KBD Window
        w(1:1024)=KBDL(2048);
        w(1025:1472)=1;
        w(1473:1600)=KBDR(256); 
        w(1601:2048)=0;  
     else                                   %SIN Window
         sin2048=sinewin(2048);
         w(1:1024)=sin2048(1:1024);
         w(1025:1472)=1;
         sin256=sinewin(256);
         w(1473:1600)=sin256(129:256); 
         w(1601:2048)=0;
     end
     %Multiply Frame with Window
    frameInTime(:,1)=frameInTime(:,1).*w;
    frameInTime(:,2)=frameInTime(:,2).*w;
    %initialize frameF
    frameF=zeros(1024,2);
    %Calculate MDCT 
    frameF(:,1)=mdct4(frameInTime(:,1));
    frameF(:,2)=mdct4(frameInTime(:,2));
    
%c. Frame Type : Eight Short Sequence   
elseif strcmp(frameType,'ESH')
    %Determine Window
    if strcmp(winType,'KBD')    %KBD Window   
            w=[KBDL(256) ; KBDR(256)];
    else                                    %SIN Window
            w=sinewin(256);   
    end
    %Initialize frameF
    frameF=zeros(128,2,8);
    %Multiply Frame with Window
    for i=1:2
     z1=frameInTime(449:704,i).*w;
     frameF(:,i,1)=mdct4(z1);
     z2=frameInTime(577:832,i).*w;
     frameF(:,i,2)=mdct4(z2);
     z3=frameInTime(705:960,i).*w;
     frameF(:,i,3)=mdct4(z3);
     z4=frameInTime(833:1088,i).*w;
     frameF(:,i,4)=mdct4(z4);
     z5=frameInTime(961:1216,i).*w;
     frameF(:,i,5)=mdct4(z5);
     z6=frameInTime(1089:1344,i).*w;
     frameF(:,i,6)=mdct4(z6);
     z7=frameInTime(1217:1472,i).*w;
     frameF(:,i,7)=mdct4(z7);
     z8=frameInTime(1345:1600,i).*w;
     frameF(:,i,8)=mdct4(z8);
    end
  
%d. Frame Type : Long Stop Sequence    
elseif strcmp(frameType,'LPS')
     %Determine Window
    w=zeros(2048,1);
    if strcmp(winType,'KBD')    %KBD Window
        w(1:448)=0;
        w(449:576)=KBDL(256);
        w(577:1024)=1;
        w(1025:2048)=KBDR(2048);
         
    else                             %SIN Window
        w(1:448)=0;
        sin256=sinewin(256);
        w(449:576)=sin256(1:128);
        w(577:1024)=1;
        sin2048=sinewin(2048);
        w(1025:2048)=sin2048(1025:2048);            
    end  
    %Multiply Frame with Window
    frameInTime(:,1)=frameInTime(:,1).*w;
    frameInTime(:,2)=frameInTime(:,2).*w;
    %initialize frameF
    frameF=zeros(1024,2);
    %Calculate MDCT 
    frameF(:,1)=mdct4(frameInTime(:,1));
    frameF(:,2)=mdct4(frameInTime(:,2));
end
end

%Window Functions 
%SIN Function
function y = sinewin(N)
x = (0:(N-1)).';
y = sin(pi*(x+0.5)/N);
end

% Kaiser-Bessel-Derived Functions
function y=KBDL(N)
%Determine a for Long or Short Window
if N==2048
    a=4;
else
    a=6;
end
w=kaiser(N/2+1,a);
y=zeros(1,N/2);
for n=1:N/2
y(1,n)=sqrt(sum(w(1:n))/sum(w(1:(N/2+1))));
end
y=y';
end

function y=KBDR(N)
%Determine a for Long or Short Window
if N==2048
    a=4;
else
    a=6;
end
w=kaiser(N/2+1,a);
y=zeros(1,N/2);
for n=1:N/2
y(n)=sqrt(sum(w(1:n))/sum(w(1:(N/2+1))));
end
y(1,:)=y(1,N/2:-1:1);
y=y';
end
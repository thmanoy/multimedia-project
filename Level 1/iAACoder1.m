function x = iAACoder1(AACSeq1, fNameOut)
[~, K] = size(AACSeq1);
original = zeros((K+1)*1024, 2);
%There is no previous frame
prevFrameT = zeros(2048, 2);
%Find 1st Frame ( frame 1)
if strcmp(AACSeq1(1).frameType, 'ESH')
    %Initialize
    frameF = zeros(128,2,8);
    %Make
    frameF(:,1,:) = AACSeq1(1).chl.frameF(:,:);
    frameF(:,2,:) = AACSeq1(1).chr.frameF(:,:);     
else
    %Initialize
    frameF = zeros(1024,2);
    %Make
    frameF(:,1) = AACSeq1(1).chl.frameF;
    frameF(:,2) = AACSeq1(1).chr.frameF;        
end
%Find 1st Frame In Time
frameT = iFilterbank(frameF, AACSeq1(1).frameType, AACSeq1(1).winType);
for i=1:K
    %Find nextFrameT (i+1 frame)
    if i == K
        % nextFrameT = [ frameT(1:1024, :) ; zeros(1024, 2) ];
        nextFrameT = zeros(2048, 2);
    else
        if strcmp(AACSeq1(i+1).frameType, 'ESH')
            %Initialize
            frameF = zeros(128,2,8);
            %Make
            frameF(:,1,:) = AACSeq1(i+1).chl.frameF(:,:);
            frameF(:,2,:) = AACSeq1(i+1).chr.frameF(:,:);
        else
            %Initialize
            frameF = zeros(1024,2);
            %Make
            frameF(:,1) = AACSeq1(i+1).chl.frameF;
            frameF(:,2) = AACSeq1(i+1).chr.frameF;        
        end
        %find next frame in time
        nextFrameT = iFilterbank(frameF, AACSeq1(i+1).frameType, AACSeq1(i+1).winType);
    end
    c=frameT;
    c(1:1024, :) = c(1:1024, :) + prevFrameT(1025:end, :);
    c(1025:end, :) = c(1025:end, :) + nextFrameT(1:1024, :);
    original((i-1)*1024+1:(i+1)*1024,:) = c;
    prevFrameT = frameT;
    frameT = nextFrameT;
end
audiowrite(fNameOut, original, 48e3);
if nargout == 1
   x = original(2049:end,:);
end
end

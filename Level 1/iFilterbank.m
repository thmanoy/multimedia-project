function frameT = iFilterbank(frameF, frameType, winType)
frameInFrequency=frameF;

%a. Frame Type : Only Long Sequence 
if strcmp(frameType,'OLS') 
    %Determine Window
    if strcmp(winType,'KBD')    %KBD Window
      w=[KBDL(2048) ; KBDR(2048)];
    else                                    %SIN Window
      w=sinewin(2048);
    end
    %initialize frameT
    frameT=zeros(2048,2);
    %Calculate iMDCT 
    frameT(:,1)=imdct4(frameInFrequency(:,1));
    frameT(:,2)=imdct4(frameInFrequency(:,2));
    %Multiply Frame with Window
    frameT(:,1)=frameT(:,1).*w;
    frameT(:,2)=frameT(:,2).*w;
    
%b. Frame Type : Long Start Sequence     
elseif strcmp(frameType,'LSS')
    %Determine Window
     w=zeros(2048,1);
     if strcmp(winType,'KBD')    %KBD Window
        w(1:1024)=KBDL(2048);
        w(1025:1472)=1;
        w(1473:1600)=KBDR(256); 
        w(1601:2048)=0;  
     else                                   %SIN Window
         sin2048=sinewin(2048);
         w(1:1024)=sin2048(1:1024);
         w(1025:1472)=1;
         sin256=sinewin(256);
         w(1473:1600)=sin256(129:256); 
         w(1601:2048)=0;
     end
    %initialize frameT
    frameT=zeros(2048,2);
    %Calculate imdct4 
    frameT(:,1)=imdct4(frameInFrequency(:,1));
    frameT(:,2)=imdct4(frameInFrequency(:,2));
    %Multiply Frame with Window
    frameT(:,1)=frameT(:,1).*w;
    frameT(:,2)=frameT(:,2).*w;
    
%c. Frame Type : Eight Short Sequence   
elseif strcmp(frameType,'ESH')
    %Determine Window
    if strcmp(winType,'KBD')    %KBD Window   
            w=[KBDL(256) ; KBDR(256)];
    else                                    %SIN Window
            w=sinewin(256);   
    end
    %Initialize z
    z=zeros(256,2,8);
    c=zeros(2048,2);
    %Calculate imdct4 and multiply frame with window
    for i=1:2
        for j=1:8
            z(:,i,j)=imdct4(frameF(:,i,j)).*w;
        end
        c(449:704,i)=c(449:704,i)+z(:,i,1);
        c(577:832,i)=c(577:832,i)+z(:,i,2);
        c(705:960,i)=c(705:960,i)+z(:,i,3);
        c(833:1088,i)=c(833:1088,i)+z(:,i,4);
        c(961:1216,i)=c(961:1216,i)+z(:,i,5);
        c(1089:1344,i)=c(1089:1344,i)+z(:,i,6);
        c(1217:1472,i)=c(1217:1472,i)+z(:,i,7);
        c(1345:1600,i)=c(1345:1600,i)+z(:,i,8);
    end
    frameT=c;  
    
%d. Frame Type : Long Stop Sequence    
elseif strcmp(frameType,'LPS')
     %Determine Window
    w=zeros(2048,1);
    if strcmp(winType,'KBD')    %KBD Window
        w(1:448)=0;
        w(449:576)=KBDL(256);
        w(577:1024)=1;
        w(1025:2048)=KBDR(2048);
         
    else                             %SIN Window
        w(1:448)=0;
        sin256=sinewin(256);
        w(449:576)=sin256(1:128);
        w(577:1024)=1;
        sin2048=sinewin(2048);
        w(1025:2048)=sin2048(1025:2048);            
    end  
    %initialize frameT
    frameT=zeros(2048,2);
    %Calculate imdct4 
    frameT(:,1)=imdct4(frameInFrequency(:,1));
    frameT(:,2)=imdct4(frameInFrequency(:,2));
    %Multiply Frame with Window
    frameT(:,1)=frameT(:,1).*w;
    frameT(:,2)=frameT(:,2).*w;
    
end
end

%Window Functions 
%SIN Function
function y = sinewin(N)
x = (0:(N-1)).';
y = sin(pi*(x+0.5)/N);
end

% Kaiser-Bessel-Derived Functions
function y=KBDL(N)
%Determine a for Long or Short Window
if N==2048
    a=4;
else
    a=6;
end
w=kaiser(N/2+1,a);
y=zeros(1,N/2);
for n=1:N/2
y(1,n)=sqrt(sum(w(1:n))/sum(w(1:(N/2+1))));
end
y=y';
end

function y=KBDR(N)
%Determine a for Long or Short Window
if N==2048
    a=4;
else
    a=6;
end
w=kaiser(N/2+1,a);
y=zeros(1,N/2);
for n=1:N/2
y(n)=sqrt(sum(w(1:n))/sum(w(1:(N/2+1))));
end
y(1,:)=y(1,N/2:-1:1);
y=y';
end
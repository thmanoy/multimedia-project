function SNR = demoAAC2(fNameIn, fNameOut)
signalBeforeCoding=padOriginalSignalForPresentation(fNameIn);
%Feed Audio to Coder and Calculate Time
fprintf('Coding : ');
tic;
AACSeq2 = AACoder2(fNameIn);
toc;
%Feed Coded Signal to Decoder and Calculate Time
fprintf('Decoding : ');
tic;
x = iAACoder2(AACSeq2, fNameOut);
toc;
%Calculate Coding error (original - coded signal) 
error=signalBeforeCoding-x;
plot(error);
%Calculate SNR
SNR(1)=snr(signalBeforeCoding(:,1),error(:,1));
SNR(2)=snr(signalBeforeCoding(:,2),error(:,2));
end

%Pads Original Signal to Compare With Coded
function Out=padOriginalSignalForPresentation(fNameIn)
% X will be a Mx2 matrix, Fs the sample rate
[Out, ~] = audioread(fNameIn);
[M, ~] = size(Out);
% Find missing samples of last frame
K = 2048-mod(M,2048);
%Pad zeros to have complete frames of 2048
Out=padarray(Out,[K 0],0,'post');
end
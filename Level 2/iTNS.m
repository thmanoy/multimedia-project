function frameFout = iTNS(frameFin, frameType, TNScoeffs)
if (strcmp(frameType,'ESH'))
    %Initialize frameFout
    frameFout=zeros(128,8);
    for i =1:8
        %Making Inverse Filter Coefficients
        iHtnsCoeff=[1;-TNScoeffs(:,i)];
        %Calculate MDCT before TNS
        frameFout(:,i)=filter(1,iHtnsCoeff,frameFin(:,i));
    end
else
    %Initialize frameFout
    frameFout=zeros(1024,1);
    %Making Inverse Filter Coefficients
     iHtnsCoeff=[1;-TNScoeffs];
     %Calculate MDCT before TNS
     frameFout=filter(1,iHtnsCoeff,frameFin);
end
end

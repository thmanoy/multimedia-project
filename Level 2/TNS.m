function [frameFout, TNScoeffs] = TNS(frameFin, frameType)
load('TableB219.mat');
if (strcmp(frameType,'ESH'))
    %1. Make Normalized MDCT coefficients
    %Initialize P(j) 
    P=zeros(42,1);
    %Initialize Sw
    Sw=zeros(128,1);
    %Initialize FrameFout
    frameFout=zeros(128,8);
    %Initialize TNScoeffs
    TNScoeffs=zeros(4,8);
    for i =1:8
        %Making P(j)
        for j=1:42
            P(j)=sum(frameFin(B219b(j,2)+1:B219b(j,3)+1,i).^2); 
            %Making Sw(k) 
            for k =(B219b(j,2)+1):(B219b(j,3)+1)
                Sw(k)=sqrt(P(j));
            end
        end
        %Apply smoothing in Sw(k) coefficients
        for k = 127:-1:1
               Sw(k)=Sw(k)+Sw(k+1);
               Sw(k)=Sw(k)/2;
        end
        for k=2:128
                Sw(k)=Sw(k)+Sw(k-1);
                Sw(k)=Sw(k)/2;
        end
        %Make Normalized MDCT coefficients
        Xw=frameFin(:,i)./Sw;
             
        %2. Calculate a coefficients
        %Find autocorrelation on Xw(k)
        autoCor=autocorr(Xw,4);
        %Calculate R & r matrix
        R=toeplitz(autoCor(1:4));
        r=autoCor(2:5);
        %Find TNScoeffs coefficients 
        TNScoeffs(:,i) = linsolve(R,r);
        [M, ~]=size(TNScoeffs);
        %3. Quantize a (TNS coefficients) 
        for j=1:M
                 TNScoeffs(j,i)=round(TNScoeffs(j,i)/0.1)*0.1;
                 if TNScoeffs(j,i)<-0.8
                    TNScoeffs(j,i)=-0.8;
                 end
                 if TNScoeffs(j,i)>0.7
                    TNScoeffs(j,i)=0.7;
                 end
        end
        %4. Apply  Htns FIR filter 
        HtnsCoeff=[1;-TNScoeffs(:,i)];
        frameFout(:,i)=filter(HtnsCoeff,1,frameFin(:,i));
    end
else
    %1. Make Normalized MDCT coefficients
    %Initialize & Making P(j) & Sw(k)
    P=zeros(69,1);
    Sw=zeros(1024,1);
    for j=1:69
        P(j)=sum(frameFin((B219a(j,2)+1):(B219a(j,3)+1)).^2); 
        %Making Sw(k)
        for k =(B219a(j,2)+1):(B219a(j,3)+1)
            Sw(k)=sqrt(P(j));
        end
    end
    %Apply smoothing in Sw(k) coefficients
    for k = 1023:-1:1
           Sw(k)=Sw(k)+Sw(k+1);
           Sw(k)=Sw(k)/2;
    end
    for k=2:1024
            Sw(k)=Sw(k)+Sw(k-1);
            Sw(k)=Sw(k)/2;
    end
    %Make Normalized MDCT coefficients
    Xw=frameFin./Sw;
    %2. Calculate a coefficients
    %Find autocorrelation on Xw(k)
    autoCor=autocorr(Xw,4);
    %Calculate R & r matrix
    R=toeplitz(autoCor(1:4));
    r=autoCor(2:5);
    %Find a coefficients 
    TNScoeffs=linsolve(R,r);
    
    %3. Quantize a (TNS coefficients) 
    for n=1:size(TNScoeffs)
             TNScoeffs(n)=round(TNScoeffs(n)/0.1)*0.1;
             if TNScoeffs(n)<-0.8
                 TNScoeffs(n)=-0.8;
             end
             if TNScoeffs(n)>0.7
                 TNScoeffs(n)=0.7;
             end
    end
    %4. Apply  Htns FIR filter 
    HtnsCoeff=[1;-TNScoeffs];
    frameFout=filter(HtnsCoeff,1,frameFin);
end
end 

function x = iAACoder2(AACSeq2, fNameOut)
[~, K] = size(AACSeq2);
original = zeros(K*2048/2, 2);
prevFrameT = zeros(2048, 2);
for i=1:2:K
    %Make nextFrameF (here temporarily named frameF)
    if i == K
        % nextFrame = [ frameT(1:1024, :) ; zeros(1024, 2) ];
        nextFrameT = zeros(2048, 2);
    else
        if strcmp(AACSeq2(i+1).frameType, 'ESH')
            frameF = zeros(128,2,8);
                frameF(:,1,:) = iTNS(AACSeq2(i+1).chl.frameF, AACSeq2(i+1).frameType, AACSeq2(i+1).chl.TNScoeffs);
                frameF(:,2,:) = iTNS(AACSeq2(i+1).chr.frameF, AACSeq2(i+1).frameType, AACSeq2(i+1).chr.TNScoeffs);          
        else
            frameF = zeros(1024,2);
            frameF(:,1) = iTNS(AACSeq2(i+1).chl.frameF, AACSeq2(i+1).frameType, AACSeq2(i+1).chl.TNScoeffs);
            frameF(:,2) = iTNS(AACSeq2(i+1).chr.frameF, AACSeq2(i+1).frameType, AACSeq2(i+1).chr.TNScoeffs);        
        end
        %Find nextFrameT
        nextFrameT = iFilterbank(frameF, AACSeq2(i+1).frameType, AACSeq2(i+1).winType);
    end
    %Make frameF 
    if strcmp(AACSeq2(i).frameType, 'ESH')
        frameF = zeros(128,2,8);
        frameF(:,1,:) = iTNS(AACSeq2(i).chl.frameF, AACSeq2(i).frameType, AACSeq2(i).chl.TNScoeffs);
        frameF(:,2,:) = iTNS(AACSeq2(i).chr.frameF, AACSeq2(i).frameType, AACSeq2(i).chr.TNScoeffs); 
    else
        frameF = zeros(1024,2);
        frameF(:,1) = iTNS(AACSeq2(i).chl.frameF, AACSeq2(i).frameType, AACSeq2(i).chl.TNScoeffs);
        frameF(:,2) = iTNS(AACSeq2(i).chr.frameF, AACSeq2(i).frameType, AACSeq2(i).chr.TNScoeffs);      
    end
    %Find FrameT
    frameT = iFilterbank(frameF, AACSeq2(i).frameType, AACSeq2(i).winType);
    frameT(1:1024, :) = frameT(1:1024, :) + prevFrameT(1025:end, :);
    frameT(1025:end, :) = frameT(1025:end, :) + nextFrameT(1:1024, :);
    original(floor(i/2)*2048+1:ceil(i/2)*2048, :) = frameT;
    prevFrameT = nextFrameT;
    frameT = nextFrameT;
end
audiowrite(fNameOut, original, 48e3);
if nargout == 1
    x = original(2049:end,:);
end
end
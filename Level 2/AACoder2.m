function AACSeq2 = AACoder2(fNameIn)
% X will be a Mx2 matrix, Fs the sample rate
[X, ~] = audioread(fNameIn);
[M, ~] = size(X);
% Find missing samples of last frame
K = 2048-mod(M,2048);
%Pad zeros to have complete frames of 2048
X=padarray(X,[K 0],0,'post');
X=[ones(2048,2);X];
%Find no of sample windows
K = ceil(M/2048);
K=K+1;
%Initialize AACSeq1 struct
AACSeq2 = struct;
for i=1:((2*K)-1)  
    %Determine FrameT and nextFrameT
     frameT=X((i-1)*1024+1:(i+1)*1024,:);
    %Check if it is the last frame
    if(i~=((2*K)-1))
        nextFrameT=X(i*1024+1:(i+2)*1024,:);
    else
        % There is no next Frame
        nextFrameT = zeros(2048,2);
    end
    %Determine prevFrameType
    if i == 1
        % There is no previous frame, but EHS has high energy, so I suppose it's OLS (low energy)
        prevFrameType = 'OLS';
    else
        prevFrameType = AACSeq2(i-1).frameType;
    end  
    %Determine Current Frame Type from SSC
    AACSeq2(i).frameType = SSC(frameT, nextFrameT, prevFrameType);
    %Set winType as sinusoid
    AACSeq2(i).winType = 'KBD';
    %Calculate Frame in Frequency after TNS and TNS coefficients
    frameF = filterbank(frameT, AACSeq2(i).frameType, AACSeq2(i).winType);
    if strcmp(AACSeq2(i).frameType, 'ESH')
            [AACSeq2(i).chl.frameF,AACSeq2(i).chl.TNScoeffs] = TNS(frameF(:,1,:),AACSeq2(i).frameType);
            [AACSeq2(i).chr.frameF,AACSeq2(i).chr.TNScoeffs]  = TNS(frameF(:,2,:),AACSeq2(i).frameType);
    else
         [AACSeq2(i).chl.frameF,AACSeq2(i).chl.TNScoeffs] = TNS(frameF(:,1),AACSeq2(i).frameType);
         [AACSeq2(i).chr.frameF,AACSeq2(i).chr.TNScoeffs]  = TNS(frameF(:,2),AACSeq2(i).frameType);
    end
end
end 
